import Bomb from "./js/bomb";
import {getRandomInt} from "./js/helpers";
import Card from "./js/card";

console.log('Hello world!');

let button = document.getElementById("addBomb");
let count = 0;
let i = 0;

button.onclick = function() {
    count += 1;
    let original = document.getElementById('bomb' + i);
    let clone = original.cloneNode(true);
    clone.id = "bomb" + ++i;
    original.parentNode.appendChild(clone);
    let clickCount = 0;

    const bomb = new Bomb(
        ['doActivate', 'doTick', 'doExplode'],
        document.getElementById('bomb'+i),
        document.getElementById('wrapper')
    );
    document.getElementById('bomb'+i).ondblclick = function() {
        count = count - 1;
        button.innerHTML = "Bombs: " + count;
    };

    document.getElementById('bomb'+i).onclick = function() {
        clickCount++;
        if (clickCount === 1) {
            setTimeout(() => {
                if (clickCount == 1) {
                    const card = new Card().addBomb(bomb);
                }
            }, 400);
        }
    };

    button.innerHTML = "Bombs: " + count;
};