import Explosives from './explosives';
import { getRandomInt } from './helpers';
export default class Bomb extends Explosives {
    constructor(actions, element, wrapper) {
        super(actions, element, wrapper);
        console.log('Bomb constructor');
        this.count = getRandomInt(10);
    }

    doActivate(callback) {
        console.log('inside doActivate');
        callback();
    }

    doTick(callback) {
        console.log('inside doTick', this.count);
        this.tick(callback);
    }

    doExplode(callback) {
        console.log('inside doExplode');
        callback();
    }

    tick(callback) {
        if (this.count === 0) {
            callback();
            return;
        }
        setTimeout(() => {
            console.log('tick', this.count);
            this.count = this.count - 1;
            this.tick(callback);
        }, 1000);
    }
}