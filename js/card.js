export default class Card {

    addBomb(bomb) {
        let list = document.getElementById('bombList');
        let getBombID = document.getElementById(bomb.element.id);
        let createLiElement = document.createElement("li");
        createLiElement.appendChild(getBombID);
        list.appendChild(createLiElement);
        this.explodeAll(bomb);
    }

    explodeAll(bomb) {
        let explodeAll = document.getElementById("destroyBombs");
        explodeAll.addEventListener('click', bomb.execute);
    }

}