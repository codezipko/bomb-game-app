import { getRandomInt, camelCaseToKebabCase } from './helpers';
export default class Explosives {
    constructor(actions, element, wrapper) {
        this.state = null;
        this.actions = actions;
        this.element = element;
        this.wrapper = wrapper;
        console.log('constructing bomb with', this.actions, this.element, this.wrapper);

        this.init();
    }

    init() {
        console.log('..initializing');
        this.create();
    }

    create() {
        console.log('..creating');
        var style = 'left: ' + getRandomInt(100) + '%; top: ' + getRandomInt(100) + '%';

        this.wrapper.appendChild(this.element);
        this.element.setAttribute('style', style);
        this.element.classList.add('visible');

        this.element.addEventListener('dblclick', this.execute);
    }

    execute = event => {
        console.log('execute is triggered', this.actions);
        let action = this.actions.shift();
        if (action) {
            console.log(action);
            this.changeState(action);
            this[action](() => {
                this.execute();
            });
        }
    }

    changeState(action) {
        this.state = action;
        this.element.setAttribute('data-action', camelCaseToKebabCase(action));
    }
}