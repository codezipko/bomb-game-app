export const getRandomInt = (max) => {
    return Math.floor(Math.random() * Math.floor(max));
}

export const camelCaseToKebabCase = (str) => {
    return str.replace(/([a-z])([A-Z])/g, '$1-$2').toLowerCase();
};
